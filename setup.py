# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

#with open('README') as f:
#    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='otherm',
    version='0.0.2',
    description='Opentherm protocol abstract representation',
    #long_description=readme,
    author='Matej Mužila',
    author_email='mmuzila@gmail.com',
    #scripts=[],
    url='https://gitlab.com/mmuzila/otherm',
    license=license,
    #include_package_data=True,
    packages=find_packages(exclude=('tests', 'docs'))
)
