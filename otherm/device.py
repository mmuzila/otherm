'''Module that represents an OpenTherm boiler. It supports both OT/+ and OT/-
modes and performs periodic communication with boiler required by the OpenTherm
standard'''

import threading
import time
from datetime import datetime
from datetime import timedelta
import queue
import sys

from . import message
import traceback as tb


ERR_WRITE_FAILED = -1
ERR_TIMED_OUT = -2
ERR_INVALID_LEN = -3
ERR_INVALID_CRC = -4
ERR_NOT_OT_PLUS = -5
ERR_INVALID_READ = -6
ERR_OK = 0

OT_OK = b'\x00'
OT_LITE_ENABLE = b'\x01'
OT_LITE_DISABLE = b'\x02'
OT_LITE_GET = b'\x03'
OT_FAIL = b'\xff'

class LiteOnly(Exception):
    '''Exception thrown when an OT/+ attempt with boiler failed, but it
    is still capable to communicate in OT/-'''
    pass
class ReadError(Exception):
    '''Exception thrown when a read operation from the boiler fails'''
    pass


class BoilerBaseClass:
    def dprint(self, text):
        try:
            if self.debug is True:
                print(text)
        except:
            pass

class Boiler(BoilerBaseClass):
    """Class to represent boiler"""

    _daemon = None

    class _Controller(BoilerBaseClass):
        '''Class to represet controller of the boiler. It is responsible
        for interaction with outer methods'''

        def __init__(self, q_send, q_recv, debug=False):
            self.dprint('Initialize __Connector')
            self._q_send = q_send
            self._q_recv = q_recv

        def send_print(self, msg):
            '''Send an OT message, receive response and print it'''
            print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')

            print(msg)

            print('-------------------------------------------')

            resp = self.send(msg)
            print(resp)

            print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')

        def send(self, msg):
            '''Send an OT message and receive response'''
            self._q_send.put(msg)
            return self._q_recv.get()

    class _Daemon(threading.Thread, BoilerBaseClass):
        '''Class to represent daemon that runs in background and does periodic
        communication with boiler required by the OpenTherm standard'''

        int_pin = 0
        end = False

        def stop(self):
            self.end = True

        def __init__(self, device, q_send, q_recv, debug=False):

            self.debug = debug


            self._device = device
            self._q_send = q_send
            self._q_recv = q_recv
            self._default_msg = message.MsgStatus()
            self._msg_factory = message.MsgFactory()

            threading.Thread.__init__(self)
            self.dprint('Initialize  _Daemon')


        def _call_normal(self, msg):

            data = OT_OK + OT_OK + msg.to_bytes()
            resp = self._call(data)

            if len(resp) != 6:
                raise ReadError

            resp = bytes(resp)

            if resp[0:2] != OT_OK + OT_OK:
                raise LiteOnly

            resp  =  self._msg_factory(resp[2:6])
            return resp


        def _call_lite(self, action):

            data = action

            # add 1 byte for boiler status and 4 in place of empty frame
            data += (0).to_bytes(5, byteorder='big')

            resp = self._call(data)

            if len(resp) != 6:
                raise ReadError

            return resp[0:1]


        def _call(self, data):

            self._device.write(data)
            return self._device.read(6, block=True, timeout=1)

        def run(self):
            try:
                self._device.read(256, block=False)
            except:
                pass


            ot_plus = False

            resp = None

            for _ in range(15):
                if self.end is True:
                    return
                try:
                    resp = self._call_normal(message.MsgStatus())
                    if isinstance(resp, message.MsgStatus) \
                            and resp.msg_type == message.MSG_READ_ACK:
                        ot_plus = True
                        break

                except Exception as e:
                    self.dprint(tb.format_exc())
                    pass

                time.sleep(1)


            # give boiler time to recover from previous communication
            time.sleep(0.1)

            if ot_plus:
                last_valid = datetime.now()

            last_attempt = datetime.now()

            while True:
                if self.end is True:
                    return
                # we must give the slave unit at least 100ms between
                # communications

                put_queue = False

                try:
                    msg = self._q_send.get(timeout=0.9)
                    put_queue = True

                    if isinstance(msg, message.MsgStatus):
                        self._default_msg = msg


                except queue.Empty:
                    msg = self._default_msg

                #print(msg)

                # do we receive valid data?
                if ot_plus and datetime.now() - last_valid > timedelta(seconds=60):
                    ot_plus = False
                    last_attempt = datetime.now()
                    self.dprint("Entering OT/- mode")

                if ot_plus:

                    try:
                        resp = self._call_normal(msg)
                        if isinstance(resp, msg.__class__):
                            last_valid = datetime.now()

                    except Exception as e:
                        self.dprint(tb.format_exc())
                        resp = e


                # fallback
                else:
                    # try to recover
                    if datetime.now() - last_attempt > timedelta(minutes=2)\
                            and put_queue is False:
                        self.dprint("Trying to recover from OT/- mode")


                        last_attempt = datetime.now()

                        # turn off heating for a while
                        try:
                            resp = self._call_lite(OT_LITE_DISABLE)
                        except:
                            pass

                        time.sleep(0.1)

                        try:
                            resp = self._call_normal(msg)
                            if resp.msg_type == message.MSG_READ_ACK:
                                ot_plus = True
                                last_valid = datetime.now()
                                self.dprint("Recovery successful")
                        except:
                            pass

                        time.sleep(0.1)


                    if self._default_msg.ch_enable:
                        data = OT_LITE_ENABLE
                    else:
                        data = OT_LITE_DISABLE

                    try:
                        resp = self._call_lite(data)
                    except Exception as e:
                        resp = e

                try:
                    if put_queue:
                        self._q_recv.put_nowait(resp)
                except queue.Full:
                    pass

                time.sleep(0.1)


    def __init__(self, device, debug=False):

        self.debug = debug
        self._q_send = queue.Queue(maxsize=1)
        self._q_recv = queue.Queue(maxsize=1)
        self._device = device
        self.start()


        self._controller = self._Controller(self._q_send, self._q_recv,\
                debug=self.debug)

    def __getattr__(self, name):
        return getattr(self._controller, name)


    def is_alive(self):
        '''Check if the daemon is alive'''
        return self._daemon.is_alive()

    def stop(self):
        '''Stop daemon it it is running'''
        if self._daemon is not None and self._daemon.is_alive():
            self._daemon.stop()
            self._daemon.join()

    def start(self):
        '''Run daemon if it is not running already'''
        if self._daemon is not None and self._daemon.is_alive():
            return
        else:
            self._daemon = self._Daemon(self._device, self._q_send,\
                    self._q_recv, debug=self.debug)
            self._daemon.setDaemon(True)
            self._daemon.start()

    def __del__(self):
        self.stop()
