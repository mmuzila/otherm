import sys
from ctypes import c_int8,  c_int16

MSG_READ_DATA    = 0
MSG_WRITE_DATA   = 1
MSG_INVALID_DATA = 2
MSG_RESERVED     = 3

MSG_READ_ACK     = 4
MSG_WRITE_ACK    = 5
MSG_DATA_INVALID = 6
MSG_UNKNOWN_DATA_ID = 7


MSG_TYPE_STR = \
{
    MSG_READ_DATA       : 'Read-Data',
    MSG_WRITE_DATA      : 'Write-Data',
    MSG_INVALID_DATA    : 'Invalid-Data',
    MSG_RESERVED        : 'Reserved',
    MSG_READ_ACK        : 'Read-Ack',
    MSG_WRITE_ACK       : 'Write-Ack',
    MSG_DATA_INVALID    : 'Data-Invalid',
    MSG_UNKNOWN_DATA_ID : 'Unknown-DataId',
}


def parity(n):
    result = 0
    while n:
        result ^= (n & 1)
        n >>= 1
    return result


def add_prop_bit(bit):

    def setx(self, value):

        value = bool(value)

        if value is True:
            self.data_value |= (1 << bit)
        else:
            self.data_value &= ~(1 << bit)

    def getx(self):
        return (self.data_value & (1 << bit)) > 0

    return property(fget=getx, fset=setx)

def add_prop_u8(offset):

    def setx(self, value):

        value = int(value)

        if value < 0 or value > 255:
            raise ValueError("setx() value in range <0,255>, {} was given"\
                    .format(value))

        self.data_value &= ~(0xff << offset)
        self.data_value |= (value << offset)

    def getx(self):
        return (self.data_value >> offset) & 0xff

    return property(fget=getx, fset=setx)

def add_prop_i8(offset):

    def setx(self, value):

        value = int(value)

        if value < -128 or value > 127:
            raise ValueError("setx() value in range <-128,127> , {} was given"\
                    .format(value))

        self.data_value &= ~(0xff << offset)
        self.data_value |= ((value << offset) & (0xff << offset))

    def getx(self):
        return c_int8((self.data_value >> offset) & 0xff).value

    return property(fget=getx, fset=setx)

def add_prop_u16():

    def setx(self, value):

        value = int(value)

        if value < 0 or value > 65535:
            raise ValueError("setx() value in range <0,65535> , {} was given"\
                    .format(value))

        self.data_value = value

    def getx(self):
        return self.data_value

    return property(fget=getx, fset=setx)

def add_prop_i16():

    def setx(self, value):

        value = int(value)

        if value < -32768 or value > 32767:
            raise ValueError("setx() value in range <-32768,32767> , {} was given"\
                    .format(value))

        self.data_value = value

    def getx(self):
        return c_int16(self.data_value).value

    return property(fget=getx, fset=setx)

def add_prop_f88():
    def setx(self, value):

        value = float(value)

        if value < -128 or value > 128:
            raise ValueError("setx() value in range <-32768,32767> , {} was given"\
                    .format(value))
        value *= 256
        value = int(value)

        self.data_value = value

    def getx(self):
        return c_int16(self.data_value).value / 256

    return property(fget=getx, fset=setx)


class MsgFactory():
    def __init__(self):



        avl_msgs = self._all_subclasses(_MsgKnownBase)
        avl_msgs = filter(lambda x: x.__name__[0] != '_', avl_msgs)
        avl_msgs = list(map(lambda x: (x._data_id, x), avl_msgs))

        self._switcher = dict(avl_msgs)

    def _all_subclasses(self, cls):
        return set(cls.__subclasses__()).union(\
            [s for c in cls.__subclasses__() for s in self._all_subclasses(c)])

    def print_supported(self):
        avl_msgs = self._all_subclasses(_MsgKnownBase)
        avl_msgs = list(filter(lambda x: x.__name__[0] != '_', avl_msgs))

        avl_msgs.sort(key = lambda x: x._data_id)

        form = '| {:<3} | {:<20} | {:<60} |'
        separator = '+=====+======================+======================='\
                '=======================================+'

        print(separator)
        print(form.format("Id", "Class Name", "Desctiption"))
        print(separator)

        separator = separator.replace('=', '-')

        for cls in avl_msgs:
            print(form.format(cls._data_id, cls.__name__, cls._name))
            print(separator)

    def __call__(self, *vargs):

        new = MsgUnknown(*vargs)

        try:
            msg_type = new.msg_type
            data_value = new.data_value
            new = self._switcher[new.data_id]()
            new.msg_type = msg_type
            new.data_value = data_value

        except KeyError:
            pass

        return new

class _MsgBase:

    msg_type = 0
    _data_id = 0
    data_value = 0
    _name = 'Base message'

    @property
    def name(self):
        return self._name

    @property
    def data_id(self):
        return self._data_id

    def to_bytes(self):

        num = self.msg_type << 28
        num |= self.data_id << 16
        num |= self.data_value
        num |= parity(num) << 31

        return num.to_bytes(4,byteorder='big')


    def __str__(self):
        data_id_str = "{} ({})".format(self.name, self.data_id)

        return "{} | {} | {}".format(MSG_TYPE_STR[self.msg_type], data_id_str,
                hex(self.data_value))

class MsgUnknown(_MsgBase):

    def __init__(self, *vargs):
        if len(vargs) != 1 and len(vargs) != 3:
            raise TypeError('Msg() takes 1 or 3 arguments but {} were given'\
                    .format(len(vargs)))

        if len(vargs) == 1:
            self._init_bytes(vargs[0])

        else:
            self._init_values(*vargs)


    def _init_bytes(self,b):

        num = int.from_bytes(b, byteorder='big')

        if parity(num) != 0:
            raise ValueError('Frame with even parity given')

        self.msg_type = (num >> 28) & 7
        self._data_id  = (num >> 16) & 0xff
        self.data_value = num & 0xffff

    def _init_values(self, msg_type, data_id, data_value):

        self.msg_type = msg_type
        self._data_id = data_id
        self.data_value = data_value


class _MsgKnownBase(_MsgBase):

    def set_attrs(self, **kvargs):

        for k in kvargs:
            getattr(self, k)
            setattr(self, k, kvargs[k])

    def __init__(self, **kvargs):
        self.set_attrs(**kvargs)

    def __str__(self):
        ret = super().__str__()



        for attr_name in dir(self):

            if attr_name in ['data_id', 'msg_type', 'data_value']:
                continue

            attr = getattr(self, attr_name)

            if type(attr) in [int, bool, float]:
                ret += "\n    {}:  {}".format(attr_name, attr)


        return ret

class _MsgKnownBaseRW(_MsgKnownBase):
    def __init__(self, **kvargs):
        if 'write' in kvargs.keys():
            if kvargs['write']:
                self.msg_type = MSG_WRITE_DATA
            del kvargs['write']

        super().__init__(**kvargs)



class MsgStatus(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 0
    _name = 'Master and Slave Status flags'

    ch_enable = add_prop_bit(8)
    dhw_enable = add_prop_bit(9)
    cooling_enable = add_prop_bit(10)
    otc_enable = add_prop_bit(11)
    ch2_enable = add_prop_bit(12)

    fault_indication = add_prop_bit(0)
    ch_mode = add_prop_bit(1)
    dhw_mode = add_prop_bit(2)
    flame_status = add_prop_bit(3)
    cooling_mode = add_prop_bit(4)
    ch2_mode = add_prop_bit(5)
    diagnostic_indication = add_prop_bit(6)

class MsgTSet(_MsgKnownBase):

    msg_type = MSG_WRITE_DATA
    _data_id = 1
    _name = 'Control Setpoint ie CH water temperature setpoint'

    temp = add_prop_f88()

class MsgMConfig(_MsgKnownBase):

    msg_type = MSG_WRITE_DATA
    _data_id = 2
    _name = 'Master Configuration Flags / Master MemberID Code'

    b0 = add_prop_bit(8)
    b1 = add_prop_bit(9)
    b2 = add_prop_bit(10)
    b3 = add_prop_bit(11)
    b4 = add_prop_bit(12)
    b5 = add_prop_bit(13)
    b6 = add_prop_bit(14)
    b7 = add_prop_bit(15)

    member_id_code_m = add_prop_u8(0)

class MsgSConfig(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 3
    _name = 'Slave Configuration Flags / Slave MemberID Code'

    dhw_present = add_prop_bit(8)
    control_type = add_prop_bit(9)
    cooling_config = add_prop_bit(10)
    dhw_config = add_prop_bit(11)
    master_pump_control = add_prop_bit(12)
    ch2_present = add_prop_bit(13)

    member_id_code_s = add_prop_u8(0)


class MsgCommand(_MsgKnownBase):
    msg_type = MSG_WRITE_DATA
    _data_id = 4
    _name = 'Remote Command'

    command_code = add_prop_u8(8)
    response_code = add_prop_u8(0)


class MsgASFFlags(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 5
    _name = 'Application-speciffic fault flags and OEM-fault-code'

    service_request = add_prop_bit(8)
    lockout_request = add_prop_bit(9)
    low_water_press = add_prop_bit(10)
    gas_flame_fault = add_prop_bit(11)
    air_press_fault = add_prop_bit(12)
    water_overtemp  = add_prop_bit(13)

class MsgRBPFlags(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 6
    _name = 'Remote boiler parameter transfer-enable & read/write flags'

    dhw_setpoint_enable = add_prop_bit(8)
    max_ch_setpoint_enable = add_prop_bit(9)

    dhw_setpoint_write = add_prop_bit(0)
    max_ch_setpoint_write = add_prop_bit(1)

class MsgRelModLevel(_MsgKnownBase):

    percent = add_prop_f88()
    msg_type = MSG_READ_DATA
    _data_id = 17
    _name = 'Relative Modulation Level'

class MsgTboiler(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 25
    _name = 'Boiler flow water temperature'

    temp = add_prop_f88()

class MsgTdhw(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 26
    _name = 'DHW temperature'

    temp = add_prop_f88()

class MsgTret(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 28
    _name = 'Return water temperature'

    temp = add_prop_f88()

class MsgTexhaust(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 33
    _name = 'Boiler exhaust temperature'

    temp = add_prop_i16()

class MsgTdhwSetBounds(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 48
    _name = 'DHW setpoint upper & lower bounds for adjustment'

    upp_bound_temp = add_prop_i8(8)
    low_bound_temp = add_prop_i8(0)

class MsgMaxTSetBounds(_MsgKnownBase):

    msg_type = MSG_READ_DATA
    _data_id = 49
    _name = 'Max CH water setpoint upper & lower bounds for adjustment'

    upp_bound_temp = add_prop_i8(8)
    low_bound_temp = add_prop_i8(0)

class MsgTdhwSet(_MsgKnownBaseRW):
    msg_type = MSG_READ_DATA
    _data_id = 56
    _name = 'DHW setpoint'

    temp = add_prop_f88()

class MsgMaxTSet(_MsgKnownBaseRW):
    msg_type = MSG_READ_DATA
    _data_id = 57
    _name = 'Max CH water setpoint'

    temp = add_prop_f88()

class MsgBurnerStarts(_MsgKnownBaseRW):
    _msg_type = MSG_READ_DATA
    _data_id = 116
    _name = 'Number of burner starts'

    starts = add_prop_u16()

class MsgCHPumpStarts(_MsgKnownBaseRW):
    _msg_type = MSG_READ_DATA
    _data_id = 117
    _name = 'Number of CH pump starts'

    starts = add_prop_u16()

class MsgDHWPumpStarts(_MsgKnownBaseRW):
    _msg_type = MSG_READ_DATA
    _data_id = 118
    _name = 'Number of DHW pump starts'

    starts = add_prop_u16()

class MsgDHWBurnerStarts(_MsgKnownBaseRW):
    _msg_type = MSG_READ_DATA
    _data_id = 119
    _name = 'Number of DHW burner starts'

    starts = add_prop_u16()


class MsgBurnerHours(_MsgKnownBaseRW):
    _msg_type = MSG_READ_DATA
    _data_id = 120
    _name = 'Number of burner operation hours'

    hours = add_prop_u16()

class MsgCHPumpHours(_MsgKnownBaseRW):
    _msg_type = MSG_READ_DATA
    _data_id = 121
    _name = 'Number of CH pump operation hours'

    hours = add_prop_u16()

class MsgDHWPumpHours(_MsgKnownBaseRW):
    _msg_type = MSG_READ_DATA
    _data_id = 122
    _name = 'Number of DHW pump operation hours'

    hours = add_prop_u16()

class MsgDHWBurnerHours(_MsgKnownBaseRW):
    _msg_type = MSG_READ_DATA
    _data_id = 123
    _name = 'Number of DHW burner operation hours'

    hours = add_prop_u16()

